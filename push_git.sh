#!/bin/sh

set -e # fail fast
set -x # print commands

git clone gitlab_repo updated_gitlab_repo
# ls gitlab_repo
# ls updated_gitlab_repo

cd updated_gitlab_repo
touch date.txt
date > date.txt
cat date.txt

git config --global user.email "saito.k.bo@gmail.com"
git config --global user.name "saito.k.bo"

git add .
git commit -m "Bumped date"
