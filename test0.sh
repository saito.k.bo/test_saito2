#!/bin/sh

set -e # fail fast
set -x # print commands

git clone gitlab_repo updated_gitlab_repo
ls gitlab_repo
ls updated_gitlab_repo

cd updated_gitlab_repo
date > date.txt
ls

git config --global user.email "saito.k.bo@gmail.com"
git config --global user.name "saito.k.bo"

git add .
git commit -m "Bumped date"

#cd updated_gitlab_repo
#mkdir updated_gitlab_repo
#cp gitlab_repo/echo_hello.sh updated_folder

