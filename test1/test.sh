#!/bin/sh

set -e # fail fast
set -x # print commands

git clone gitlab_repo updated_gitlab_repo

cd updated_gitlab_repo
mkdir test2
date > test2.test_date.txt
cd ..

git config --global user.email "saito.k.bo@gmail.com"
git config --global user.name "saito.k.bo"

git add .
git commit -m "Putting test_date"
